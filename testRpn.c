#include "testRpn.h"

#include <math.h>
#include <stdio.h>

#include "rpn.h"
#include "testCommon.h"

/* main関数の記述部 */
int main() {
    testInitStack();
    testHeadValue();
    testPush();
    testPull();
    testPrintStack();
    testAddStack();
    testMulStack();
    testSubStack();
    testDivStack();
    testErrorCheck();
    return 0;
}
