#include <math.h>
#include <stdio.h>

#include "rpn.h"
#include "testCommon.h"

void testInitStack() {
    stack a;
    testStart("initStack");
    a.head = 0;
    initStack(&a, N);
    assertEqualsInt(a.head, N);
}

void testHeadValue() {
    stack a;
    double x;
    testStart("headValue");
    initStack(&a, N);
    x = headValue(&a);
    assertEqualsInt(isnan(x), 1);
    a.head--;
    a.data[a.head] = 2.0;
    x = headValue(&a);
    assertEqualsDouble(x, 2.0);
    a.head--;
    a.data[a.head] = M_PI;
    x = headValue(&a);
    assertEqualsDouble(x, M_PI);
}

void testPush(void) {
    stack a;
    double x;
    testStart("push");
    initStack(&a, N);
    push(&a, 2.0);
    x = headValue(&a);
    assertEqualsDouble(x, 2.0);
    assertEqualsInt(a.head, N - 1);
    push(&a, M_PI);
    x = headValue(&a);
    assertEqualsDouble(x, M_PI);
    assertEqualsInt(a.head, N - 2);
}

void testPull(void) {
    stack a;
    double x;
    testStart("pull");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    x = pull(&a);
    assertEqualsDouble(x, M_PI);
    x = pull(&a);
    assertEqualsDouble(x, 2.0);
    x = pull(&a);
    assertNotEqualsInt(isnan(x), 0);
}

void testPrintStack() {
    stack a;
    testStart("printStack");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    push(&a, sqrt(2.0));
    printStack(&a);
    printf("1行空き\n");
    pull(&a);
    printStack(&a);
    printf("1行空き\n");
    push(&a, sqrt(2.0));
    push(&a, sqrt(3.0));
    printStack(&a);
}

void testAddStack() {
    stack a;
    testStart("addStack");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    push(&a, sqrt(2.0));
    addStack(&a);
    assertEqualsDouble(headValue(&a), sqrt(2.0) + M_PI);
    addStack(&a);
    assertEqualsDouble(headValue(&a), sqrt(2.0) + M_PI + 2.0);
    addStack(&a);
    assertNotEqualsInt(isnan(headValue(&a)), 0);
}

void testMulStack() {
    stack a;
    testStart("mulStack");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    push(&a, sqrt(2.0));
    mulStack(&a);
    assertEqualsDouble(headValue(&a), sqrt(2.0) * M_PI);
    mulStack(&a);
    assertEqualsDouble(headValue(&a), sqrt(2.0) * M_PI * 2.0);
    mulStack(&a);
    assertNotEqualsInt(isnan(headValue(&a)), 0);
}

void testSubStack() {
    stack a;
    testStart("subStack");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    push(&a, sqrt(2.0));
    subStack(&a);
    assertEqualsDouble(headValue(&a), M_PI - sqrt(2.0));
    subStack(&a);
    assertEqualsDouble(headValue(&a), 2.0 - (M_PI - sqrt(2.0)));
    subStack(&a);
    assertNotEqualsInt(isnan(headValue(&a)), 0);
}

void testDivStack() {
    stack a;
    testStart("divStack");
    initStack(&a, N);
    push(&a, 2.0);
    push(&a, M_PI);
    push(&a, sqrt(2.0));
    divStack(&a);
    assertEqualsDouble(headValue(&a), M_PI / sqrt(2.0));
    divStack(&a);
    assertEqualsDouble(headValue(&a), 2.0 / (M_PI / sqrt(2.0)));
    divStack(&a);
    assertNotEqualsInt(isnan(headValue(&a)), 0);
}