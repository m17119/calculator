// X班 complex.c
#include <stdio.h>
#include <math.h>
#include "rpn.h"

void initStack(stack *sp, int n){
    sp->head = n;

}

double headValue(stack *sp){
    if (sp->head == N) {
        return sqrt(-1.0);
    }else{
        return sp->data[sp->head];
    }
}

void push(stack *sp, double x){
    sp->head--;
    sp->data[sp->head] = x;
}

double pull(stack *sp){
    double ans;
    if (sp->head == N) {
        return sqrt(-1.0);
    }else{
        ans = headValue(sp);
        sp->head++;
        return ans;
    }
}

void printStack(stack *sp){
    int i;
    for (i = N - 1; i >= sp->head; i--) {
        printf("%lf\n", sp->data[i]);
    }
}

void addStack(stack *sp){
    push(sp, pull(sp) + pull(sp));
   }

void mulStack(stack *sp){
    push(sp, pull(sp) * pull(sp));
}

void subStack(stack *sp){
    double x = pull(sp);
    push(sp, pull(sp) - x);
}

void divStack(stack *sp){
    double x = pull(sp);
    push(sp, pull(sp) / x);
}
