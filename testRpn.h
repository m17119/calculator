#ifndef test_rpn_h
#define test_rpn_h

void testInitStack(void);
void testHeadValue(void);
void testPush(void);
void testPull(void);
void testPrintStack(void);
void testAddStack(void);
void testMulStack(void);
void testSubStack(void);
void testDivStack(void);

#endif
