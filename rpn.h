#ifndef rpn_h
#define rpn_h

#define N 10000

typedef struct {
    double data[N];
    int head;
} stack;

typedef struct {
    int data[N];
    int head;
} stackI;

void initStack(stack *sp, int n);
double headValue(stack *sp);
void push(stack *sp, double x);
double pull(stack *sp);
void printStack(stack *sp);
void addStack(stack *sp);
void mulStack(stack *sp);
void subStack(stack *sp);
void divStack(stack *sp);

void initStackI(stackI *sp, int n);
double headValueI(stackI *sp);
void pushI(stackI *sp, int x);
int pullI(stackI *sp);
void printStackI(stackI *sp);
void addStackI(stackI *sp);
void mulStackI(stackI *sp);
void subStackI(stackI *sp);
void divStackI(stackI *sp);
void printStackI(stackI *sp);
void soinsubunkai(int x);
void ss(int x, int y);
#endif
