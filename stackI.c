// X班 complex.c
#include <stdio.h>
#include <math.h>
#include "rpn.h"

void initStackI(stackI *sp, int n){
  sp->head = n;

}

double headValueI(stackI *sp){
  if (sp->head == N) {
    return sqrt(-1.0);
  }else{
    return sp->data[sp->head];
  }
}

void pushI(stackI *sp, int x){
  sp->head--;
  sp->data[sp->head] = x;
}

int pullI(stackI *sp){
  int ans;
  if (sp->head == N) {
    return sqrt(-1.0);
  }else{
    ans = headValueI(sp);
    sp->head++;
    return ans;
  }
}

void printStackI(stackI *sp){
  int i;
  for (i = N - 1; i >= sp->head; i--) {
    printf("%d\n", sp->data[i]);
  }
}

void addStackI(stackI *sp){
  pushI(sp, pullI(sp) + pullI(sp));
}

void mulStackI(stackI *sp){
  pushI(sp, pullI(sp) * pullI(sp));
}

void subStackI(stackI *sp){
  int x = pullI(sp);
  pushI(sp, pullI(sp) - x);
}

void divStackI(stackI *sp){
  int x = pullI(sp);
  pushI(sp, pullI(sp) / x);
}

void soinsubunkai(int x) {
  int data[x];
  int ruijou[x];
  int ooo[x];
  int i, j = 0, count = 0, k = 0, s = 0, countII = 0, ori = x;

  for (i = 0; i < x; i++) {
    data[i] = 0;
    ruijou[i] = 0;
    ooo[i] = 0;
  }

  for (i = 2; i <= x; i++) {
    if (x % i == 0) {
      x = x / i;
      data[j] = i;
      j += 1;
      i -= 1;
      count += 1;
    }
  }

  int dataX[count];
  int ruijouX[count];
  int oooX[count];
  for (j = 0; j < count; j++) {
    dataX[j] = data[j];
    ruijouX[j] = ruijou[j];
    oooX[j] = ooo[j];
  }

  if (count == 1) {
    printf("%dは素数です\n", ori);
  } else {
    for (j = s; j < count; j++) {
      if (dataX[j] == dataX[j+1]) {
        ruijouX[k] += 1;
      } else {
        oooX[k] = dataX[j];
        s = j+1;
        countII += 1;
        k += 1;
      }
    }

    printf("%d = ", ori);
    for (k = 0; k < countII; k++) {
      if (ruijouX[k] == 0) {
        printf("%d", oooX[k]);
      } else {
        printf("%d^%d", oooX[k], ruijouX[k]+1);
      }
      if (k != countII-1) {
        printf(" × ");
      }
    }
    printf("\n");
  }
}

void ss(int x, int y) {
  int data[x];
  int i, j = 0, count = 0, oriX = x, oriY = y;

  for (i = 0; i < x; i++) {
    data[i] = 0;
  }

  for (i = 2; i <= x; i++) {
    if (x % i == 0 && y % i == 0) {
      x = x / i;
      y = y / i;
      data[j] = i;
      j += 1;
      i -= 1;
      count += 1;
    }
  }

  int dataX[count];
  for (i = 0; i < count; i++) {
    dataX[i] = data[i];
  }

  if (count == 0) {
    printf("%dと%dの最大公約数： 1\n", oriX, oriY);
    printf("%dと%dの最小公倍数： %d\n", oriX, oriY, x * y);
  } else {
    for (j = 0; j < count; j++) {
      if (j != count-1) {
        dataX[j+1] *= dataX[j];
      }
    }
    printf("%dと%dの最大公約数： %d\n", oriX, oriY, dataX[count-1]);
    printf("%dと%dの最小公倍数： %d\n", oriX, oriY, dataX[count-1] * x * y);
  }
}
