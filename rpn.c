// p班 rpn.c
#include "rpn.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "math.h"
#define LEN 200

// このファイルは 3314 Koike Iori が記述.
int main() {
    char line[LEN];
    stack a;
    stackI b;

    initStack(&a, N);
    initStackI(&b, N);
    fprintf(stderr,
            " d: 一番下の数字を破棄します。 e: 電卓機能をやめます。以下演算:+, "
            "-, *, /, もしくは小数点第六位までの数値\n");
    fprintf(stderr, "i: 整数モード, f: 小数モード\n");

    while (1) {
        printf("モード選択\n");
        fgets(line, LEN, stdin);
        if (strcmp(line, "f\n") == 0) {
            printf("小数モード\n");
            while (1) {
                fgets(line, LEN, stdin);
                if (strcmp(line, "+\n") == 0) {
                    addStack(&a);
                } else if (strcmp(line, "*\n") == 0) {
                    mulStack(&a);
                } else if (strcmp(line, "-\n") == 0) {
                    subStack(&a);
                } else if (strcmp(line, "/\n") == 0) {
                    divStack(&a);
                } else if (strcmp(line, "d\n") == 0) {
                    printf("%fを捨てました\n", pull(&a));
                } else if (strcmp(line, "e\n") == 0) {
                    break;
                } else {
                    push(&a, atof(line));
                }
                if (isnan(headValue(&a)) != 0) {
                    printf("スタックが空かまたは最後の演算がエラーです\n");
                }
                printf("_________\n");
                printStack(&a);
                printf("‾‾‾‾‾‾‾‾‾\n");
            }
        } else if (strcmp(line, "i\n") == 0) {
            printf("整数モード\n");
            printf("s: 素因数分解 ss: 二つの値の最大公約数、最小公倍数\n");
            while (1) {
                fgets(line, LEN, stdin);
                if (strcmp(line, "+\n") == 0) {
                    addStackI(&b);
                } else if (strcmp(line, "*\n") == 0) {
                    mulStackI(&b);
                } else if (strcmp(line, "-\n") == 0) {
                    subStackI(&b);
                } else if (strcmp(line, "/\n") == 0) {
                    divStackI(&b);
                } else if (strcmp(line, "d\n") == 0) {
                    printf("%dを捨てました\n", pullI(&b));
                } else if (strcmp(line, "e\n") == 0) {
                    break;
                } else if (strcmp(line, "s\n") == 0) {
                    soinsubunkai(headValueI(&b));
                    pullI(&b);
                } else if (strcmp(line, "ss\n") == 0) {
                    ss(pullI(&b), pullI(&b));
                } else {
                    pushI(&b, atof(line));
                }
                if (isnan(headValueI(&b)) != 0) {
                    printf(
                        "stderr: スタックが空かまたは最後の演算がエラーです\n");
                }
                printf("_________\n");
                printStackI(&b);
                printf("‾‾‾‾‾‾‾‾‾\n");
            }
        } else if (strcmp(line, "e\n") == 0) {
            break;
        }
    }
    return 0;
}
